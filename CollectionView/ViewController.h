//
//  ViewController.h
//  CollectionView
//
//  Created by Rennel Sangria on 6/9/15.
//  Copyright (c) 2015 Rennel Sangria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InformationViewController.h"

@interface ViewController : UIViewController<UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>
{
    NSArray* arrayOfRecipes;
}


@end

