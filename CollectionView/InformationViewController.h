//
//  InformationViewController.h
//  CollectionView
//
//  Created by Rennel Sangria on 6/15/15.
//  Copyright (c) 2015 Rennel Sangria. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InformationViewController : UIViewController
@property(nonatomic, strong) NSDictionary* informationDictionary;

@end
