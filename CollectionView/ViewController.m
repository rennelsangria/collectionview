//
//  ViewController.m
//  CollectionView
//
//  Created by Rennel Sangria on 6/9/15.
//  Copyright (c) 2015 Rennel Sangria. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()



@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    //set the path of the plist
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Property List" ofType:@"plist"];
    //Build the array from the plist
    arrayOfRecipes = [[NSArray alloc] initWithContentsOfFile:path];
    
    //create flow layout
    UICollectionViewFlowLayout* vfl= [[UICollectionViewFlowLayout alloc]init];
    
    //UICollectionView* collectionView = [[UICollectionView alloc]initWithFrame:self.view.bounds collectionViewLayout:flowLayout];
    
    UICollectionView* cv = [[UICollectionView alloc]initWithFrame:CGRectMake(0,20,self.view.frame.size.width, self.view.frame.size.height)collectionViewLayout:vfl];
    
    cv.delegate = self;
    cv.dataSource = self;
    
    [cv registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
    
    [self.view addSubview:cv];
    
    
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *identifier = @"cell";
    
    
    UICollectionViewCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
  
    //put the array of recipes in a dictionary
    NSDictionary* infoDictionary = arrayOfRecipes[indexPath.row];
    UIImage* image = [UIImage imageNamed:[infoDictionary objectForKey:@"Image"]];
    UIImageView* recipeImageView = [[UIImageView alloc]initWithImage:image];

    
    //recipeImageView.image = [UIImage imageNamed:[infoDictionary objectForKey:@"Image"]];
    
    
    
    cell.backgroundColor = [UIColor greenColor];
    
    [cell addSubview:recipeImageView];
    
    
    


    [cell.layer setBorderWidth:2.0f];
    [cell.layer setBorderColor:[UIColor whiteColor].CGColor];
    return cell;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [arrayOfRecipes count];
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(self.view.frame.size.width- 54, 240);//(self.view.frame.size.width- 20, 250);
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(10, 10, 10, 10);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    //NSLog(@"indexpath %@", indexPath);
    NSDictionary* infoDictionary = arrayOfRecipes[indexPath.row];
    InformationViewController* info = [InformationViewController new];
    info.informationDictionary= infoDictionary;
    [self.navigationController pushViewController:info animated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
